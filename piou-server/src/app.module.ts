import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { NewsModule } from "./news/news.module";
import { UsersModule } from "./users/users.module";
import { LevelsModule } from "./levels/levels.module";
import { TriesModule } from "./tries/tries.module";
import { AuthModule } from "./auth/auth.module";
import configuration from "./configuration";
import { AuthService } from "./auth/auth.service";
import { User } from "./users/entities/user.entity";
import { Try } from "./tries/entities/try.entity";
import { Level } from "./levels/entities/level.entity";
import { News } from "./news/entities/news.entity";
import { XmlModule } from "./xml/xml.module";
import { ViewsModule } from "./views/views.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath:
        process.env.NODE_ENV !== "production"
          ? ["../.env.development"]
          : ["../.env.production"],
      load: [configuration],
      isGlobal: true,
      expandVariables: true,
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        type: "postgres",
        host: configService.getOrThrow<string>("database.host"),
        port: configService.getOrThrow<number>("database.port"),
        username: configService.getOrThrow<string>("database.username"),
        password: configService.getOrThrow<string>("database.password"),
        database: configService.getOrThrow<string>("database.name"),
        autoLoadEntities: true,
        synchronize: false, // !configService.get("production"),
        logging: !configService.getOrThrow<boolean>("production"),
      }),
      inject: [ConfigService],
    }),
    NewsModule,
    UsersModule,
    LevelsModule,
    TriesModule,
    AuthModule,
    XmlModule,
    ViewsModule,

    // AdminJS version 7 is ESM-only. In order to import it, you have to use dynamic imports.
    Promise.all([
      import("@adminjs/nestjs"),
      import("@adminjs/typeorm"),
      import("adminjs"),
    ]).then(
      ([{ AdminModule }, { Database, Resource }, { default: AdminJS }]) => {
        AdminJS.registerAdapter({ Resource, Database });

        return AdminModule.createAdminAsync({
          useFactory: (
            configService: ConfigService,
            authService: AuthService,
          ) => ({
            adminJsOptions: {
              rootPath: "/admin",
              resources: [
                News,
                { resource: User, options: { titleProperty: "displayName" } },
                Level,
                Try,
              ],
              branding: {
                companyName: "PiouPiouZ - Admin",
                withMadeWithLove: false,
              },
              locale: {
                language: "en",
                translations: {
                  messages: {
                    loginWelcome: "",
                  },
                  labels: {
                    loginWelcome: "PiouPiouZ Administration",
                  },
                },
              },
            },
            auth: {
              authenticate: async (email: string, password: string) => {
                // TODO: Automatic login (and login for EternalTwin oauth
                const user = await authService.validateUser(email, password);
                if (!user) return null;
                if (!user.isAdmin) return null;
                return { email: user.userName, id: user.id };
              },
              cookieName: configService.getOrThrow<string>("admin.cookieName"),
              cookiePassword:
                configService.getOrThrow<string>("admin.cookieSecret"),
            },
            sessionOptions: {
              resave: false,
              saveUninitialized: false,
              secret: configService.getOrThrow<string>("admin.sessionSecret"),
            },
          }),
          inject: [ConfigService, AuthService],
          imports: [AuthModule],
        });
      },
    ),
  ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor() {}
}
