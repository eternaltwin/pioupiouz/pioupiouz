import { Module } from "@nestjs/common";
import { XmlController } from "./xml.controller";
import { LevelsModule } from "../levels/levels.module";
import { TriesModule } from "../tries/tries.module";
import { UsersModule } from "../users/users.module";

@Module({
  controllers: [XmlController],
  imports: [LevelsModule, TriesModule, UsersModule],
})
export class XmlModule {}
