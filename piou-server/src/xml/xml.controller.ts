import {
  Controller,
  Get,
  Header,
  Param,
  UnauthorizedException,
  UseGuards,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { AuthenticatedGuard } from "../auth/authenticated.guard";
import { Medal, PiouPiouzLevel, PlayMode } from "piou-core/types";
import { Level } from "../levels/entities/level.entity";
import { Try } from "../tries/entities/try.entity";
import { buildXMLLevel } from "piou-core/encoding";
import { LevelsService } from "../levels/levels.service";
import { TriesService } from "../tries/tries.service";
import { ReqUser } from "../helpers";
import { User } from "../users/entities/user.entity";
import { UpdateUserFromDbPipe } from "../helpers.pipe";

function levelToXml(
  level: Level,
  {
    preview,
    tryy,
    tuto,
    cards,
  }: {
    tryy?: Try;
    tuto?: string;
    preview?: Medal;
    cards?: [number, number][];
  } = {},
): string {
  const l: PiouPiouzLevel = {
    data: level.data,
    soluce: tryy?.soluce,
    tuto: tuto,
    cards: cards?.map((c) => c.join(":")).join(","),
    _piouz: level.piouz,
    _mode:
      preview !== undefined
        ? PlayMode.Preview
        : cards
          ? PlayMode.Edit
          : tryy
            ? PlayMode.Replay
            : PlayMode.Normal,
    _time: level.time,
    _title: level.title,
    _status: preview,
    _id: 0, // We do not use them
    _playId: 0,
  };

  return buildXMLLevel(l);
}

function getMedal(level: Level, tryy: Try | null): Medal {
  if (tryy === null) return Medal.None;
  if (tryy.piouz === 0) return Medal.Dead;
  if (tryy.piouz < level.piouz) return Medal.Silver;
  if (tryy.piouz === level.piouz) return Medal.Gold;

  return Medal.Star;
}

@ApiTags("xml")
@UseGuards(AuthenticatedGuard)
@Controller("xml")
export class XmlController {
  constructor(
    private readonly levelsService: LevelsService,
    private readonly triesService: TriesService,
  ) {}

  @Get("level/:id")
  @Header("content-type", "text/xml")
  async getLevel(
    @Param("id") id: string,
    @ReqUser(UpdateUserFromDbPipe) user: User,
  ): Promise<string> {
    const level = await this.levelsService.findOne(user, id);
    return levelToXml(level);
  }

  @Get("level/:id/preview")
  @Header("content-type", "text/xml")
  async getPreview(
    @Param("id") id: string,
    @ReqUser(UpdateUserFromDbPipe) user: User,
  ): Promise<string> {
    const level = await this.levelsService.findOne(user, id);
    const tryy = await this.triesService.findOneOf(user, id);
    return levelToXml(level, {
      tryy: tryy ?? undefined,
      preview: getMedal(level, tryy),
    });
  }

  @Get("level/:id/edit")
  @Header("content-type", "text/xml")
  async getEdit(
    @Param("id") id: string,
    @ReqUser(UpdateUserFromDbPipe) user: User,
  ): Promise<string> {
    const level = await this.levelsService.findOne(user, id);
    if (level.isPublished)
      throw new UnauthorizedException("You can't edit a published level");
    return levelToXml(level, { cards: [[0, 1]] });
  }

  @Get("try/:id")
  @Header("content-type", "text/xml")
  async getTry(
    @Param("id") id: string,
    @ReqUser(UpdateUserFromDbPipe) user: User,
  ): Promise<string> {
    const tryy = await this.triesService.findOne(user, id);
    const level = await this.levelsService.findOne(user, tryy.levelId);
    return levelToXml(level);
  }
}
