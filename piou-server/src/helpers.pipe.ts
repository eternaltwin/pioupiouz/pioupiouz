import { PipeTransform, Injectable, ArgumentMetadata } from "@nestjs/common";
import { UsersService } from "./users/users.service";
import { SessionUser, User } from "./users/entities/user.entity";

@Injectable()
export class UpdateUserFromDbPipe
  implements PipeTransform<User | null, Promise<User | null>>
{
  constructor(private userService: UsersService) {}

  async transform(
    user: SessionUser | null,
    metadata: ArgumentMetadata,
  ): Promise<User | null> {
    if (user?.id === undefined) return null;
    return this.userService.findOne({ id: user.id });
  }
}
