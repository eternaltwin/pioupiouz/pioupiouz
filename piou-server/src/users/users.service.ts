import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { User } from "./entities/user.entity";
import { paginate, PaginateQuery } from "nestjs-paginate";
import * as bcrypt from "bcrypt";

// @ts-expect-error ES Module import (type only)
import type { ShortUser } from "@eternaltwin/core/user/short-user";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  async create(username: string, password: string) {
    return this.userRepository.save(
      User.create<User>({
        userName: username,
        password: await bcrypt.hash(password, 10),
        displayName: username,
      }),
    );
  }

  async createWithEternalTwin(eternalTwinUser: ShortUser) {
    return this.userRepository.save(
      User.create<User>({
        displayName: eternalTwinUser.displayName.current.value,
        eternalTwinId: eternalTwinUser.id,
      }),
    );
  }

  async findAll(query: PaginateQuery) {
    return paginate(query, this.userRepository, {
      sortableColumns: ["createdAt"],
      defaultSortBy: [["createdAt", "DESC"]],
      select: ["id", "displayName", "createdAt"],
    });
  }

  async findOne({
    id,
    username,
    eternalTwinId,
  }: {
    id?: string;
    username?: string;
    eternalTwinId?: string;
  }) {
    if (id === undefined && username === undefined && eternalTwinId === null)
      return null;

    return this.userRepository.findOneBy({
      id,
      userName: username,
      eternalTwinId,
    });
  }

  async associateEternalTwinId(user: User, eternalTwinId: string) {
    if (user.eternalTwinId)
      throw new BadRequestException(
        "User already linked to an EternalTwin account. Please contact an administrator to unlink.",
      );
    user.eternalTwinId = eternalTwinId;
    return this.userRepository.save(user);
  }

  async updateUserStats(user: User) {
    const result = await this.userRepository
      .createQueryBuilder("user")
      .leftJoin("user.tries", "try")
      .leftJoin("try.level", "level")
      // All saved piouz
      .select("SUM(try.piouz)", "piouz")
      // Dead medals (piouz = 0)
      .addSelect("SUM(CASE WHEN try.piouz = 0 THEN 1 ELSE 0 END)", "levelDead")
      // Silver medals (piouz < level.piouz)
      .addSelect(
        "SUM(CASE WHEN try.piouz > 0 AND try.piouz < level.piouz THEN 1 ELSE 0 END)",
        "levelSilver",
      )
      // Gold medals (piouz = level.piouz)
      .addSelect(
        "SUM(CASE WHEN try.piouz = level.piouz THEN 1 ELSE 0 END)",
        "levelGold",
      )
      // Star medals (piouz > level.piouz)
      .addSelect(
        "SUM(CASE WHEN try.piouz > level.piouz THEN 1 ELSE 0 END)",
        "levelStar",
      )
      .where("user.id = :id", { id: user.id })
      .getRawOne();

    user.savedPiouz = result.piouz ?? 0;
    user.medals = [
      result.levelDead,
      result.levelSilver,
      result.levelGold,
      result.levelStar,
    ];
    await this.userRepository.save(user);
  }
}
