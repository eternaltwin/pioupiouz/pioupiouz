import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Level } from "../../levels/entities/level.entity";
import { Try } from "../../tries/entities/try.entity";
import { News } from "../../news/entities/news.entity";

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;
  @Column({ length: 255 })
  displayName: string;

  @Column({ length: 255, nullable: true, unique: true })
  userName: string;
  @Column({ length: 255, nullable: true })
  password: string;

  @Column({ length: 255, nullable: true, unique: true })
  eternalTwinId: string;

  @Column({ default: false })
  help: boolean;

  @Column("smallint", { default: 0 })
  savedPiouz: number;
  @Column("smallint", { array: true, default: [0, 0, 0, 0] }) // [ dead, silver, gold, star ]
  medals: number[];

  @CreateDateColumn()
  createdAt: Date;

  @Column({ default: false })
  isAdmin: boolean;

  @OneToMany(() => Level, (level) => level.author)
  levels: Level[];
  @OneToMany(() => Try, (tryy) => tryy.user)
  tries: Try[];
  @OneToMany(() => News, (news) => news.author)
  news: News[];
}

export class SessionUser {
  id: string;
  userName: string;
  displayName: string;
  eternalTwinId: string;
  isAdmin: boolean;
}
