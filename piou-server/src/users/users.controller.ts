import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  UseGuards,
} from "@nestjs/common";
import { UsersService } from "./users.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { ApiTags } from "@nestjs/swagger";
import { Paginate, PaginateQuery } from "nestjs-paginate";
import { PaginateQueryOptions, ReqUser } from "../helpers";
import { AuthenticatedGuard } from "../auth/authenticated.guard";
import { User } from "./entities/user.entity";
import { UpdateUserFromDbPipe } from "../helpers.pipe";

@ApiTags("users")
@UseGuards(AuthenticatedGuard)
@Controller("api/users")
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    if (createUserDto.password !== createUserDto.confirmPassword) {
      throw new BadRequestException("Passwords do not match");
    }
    return this.usersService.create(
      createUserDto.username,
      createUserDto.password,
    );
  }

  @Get()
  @PaginateQueryOptions(User)
  findAll(@Paginate() query: PaginateQuery) {
    return this.usersService.findAll(query);
  }

  @Get("me")
  @UseGuards(AuthenticatedGuard)
  me(@ReqUser(UpdateUserFromDbPipe) user: User) {
    return user;
  }

  @Get(":id")
  findOne(@Param("id") id: string): Promise<User> {
    return this.usersService.findOne({ id }) as Promise<User>;
  }

  // TODO: EternalTwin route(s)
}
