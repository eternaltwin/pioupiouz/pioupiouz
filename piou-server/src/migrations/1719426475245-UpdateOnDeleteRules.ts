import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateOnDeleteRules1719426475245 implements MigrationInterface {
  name = "UpdateOnDeleteRules1719426475245";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "try" DROP CONSTRAINT "FK_feb4e8da5bbaf3537c3de6d9b9f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" DROP CONSTRAINT "FK_0f65a029b9f34234fa83e917c51"`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" DROP CONSTRAINT "FK_09cc391a19d7d701da8dc4f07eb"`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" DROP CONSTRAINT "FK_762c9a85cfac5e31d8e77abcac3"`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" DROP CONSTRAINT "FK_67bab4ab2ebc496a545c586b2e1"`,
    );
    await queryRunner.query(
      `ALTER TABLE "news" DROP CONSTRAINT "FK_18ab67e7662dbc5d45dc53a6e00"`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" ADD CONSTRAINT "FK_feb4e8da5bbaf3537c3de6d9b9f" FOREIGN KEY ("levelId") REFERENCES "level"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" ADD CONSTRAINT "FK_0f65a029b9f34234fa83e917c51" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" ADD CONSTRAINT "FK_09cc391a19d7d701da8dc4f07eb" FOREIGN KEY ("soluceOfId") REFERENCES "level"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" ADD CONSTRAINT "FK_762c9a85cfac5e31d8e77abcac3" FOREIGN KEY ("authorId") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" ADD CONSTRAINT "FK_67bab4ab2ebc496a545c586b2e1" FOREIGN KEY ("soluceId") REFERENCES "try"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "news" ADD CONSTRAINT "FK_18ab67e7662dbc5d45dc53a6e00" FOREIGN KEY ("authorId") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "news" DROP CONSTRAINT "FK_18ab67e7662dbc5d45dc53a6e00"`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" DROP CONSTRAINT "FK_67bab4ab2ebc496a545c586b2e1"`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" DROP CONSTRAINT "FK_762c9a85cfac5e31d8e77abcac3"`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" DROP CONSTRAINT "FK_09cc391a19d7d701da8dc4f07eb"`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" DROP CONSTRAINT "FK_0f65a029b9f34234fa83e917c51"`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" DROP CONSTRAINT "FK_feb4e8da5bbaf3537c3de6d9b9f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "news" ADD CONSTRAINT "FK_18ab67e7662dbc5d45dc53a6e00" FOREIGN KEY ("authorId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" ADD CONSTRAINT "FK_67bab4ab2ebc496a545c586b2e1" FOREIGN KEY ("soluceId") REFERENCES "try"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" ADD CONSTRAINT "FK_762c9a85cfac5e31d8e77abcac3" FOREIGN KEY ("authorId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" ADD CONSTRAINT "FK_09cc391a19d7d701da8dc4f07eb" FOREIGN KEY ("soluceOfId") REFERENCES "level"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" ADD CONSTRAINT "FK_0f65a029b9f34234fa83e917c51" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" ADD CONSTRAINT "FK_feb4e8da5bbaf3537c3de6d9b9f" FOREIGN KEY ("levelId") REFERENCES "level"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
