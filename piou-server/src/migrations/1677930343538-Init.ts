import { MigrationInterface, QueryRunner } from "typeorm";

export class Init1677930343538 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "try" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "soluce" character varying NOT NULL, "time" integer NOT NULL, "piouz" integer NOT NULL, "levelId" uuid NOT NULL, "userId" uuid NOT NULL, "soluceOfId" uuid, CONSTRAINT "UQ_fc9a555724884e099006c6d33f1" UNIQUE ("userId", "levelId"), CONSTRAINT "REL_09cc391a19d7d701da8dc4f07e" UNIQUE ("soluceOfId"), CONSTRAINT "PK_e72615c2bda294ec22a432f53f6" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "news" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "title" character varying NOT NULL, "img" character varying NOT NULL DEFAULT '/gfx/news/news_img.jpg', "content" character varying NOT NULL, "authorId" uuid NOT NULL, CONSTRAINT "PK_39a43dfcb6007180f04aff2357e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "displayName" character varying(255) NOT NULL, "userName" character varying(255), "password" character varying(255), "eternalTwinId" character varying(255), "help" boolean NOT NULL DEFAULT false, "savedPiouz" smallint NOT NULL DEFAULT '0', "medals" smallint array NOT NULL DEFAULT '{0,0,0,0}', "createdAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_da5934070b5f2726ebfd3122c80" UNIQUE ("userName"), CONSTRAINT "UQ_8cca531ce3829cc0a0f3e4ac202" UNIQUE ("eternalTwinId"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "level" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "authorId" uuid NOT NULL, "isPublished" boolean NOT NULL DEFAULT false, "publishedAt" TIMESTAMP, "theme" smallint NOT NULL DEFAULT '0', "title" character varying(255) NOT NULL, "description" character varying NOT NULL DEFAULT '', "data" character varying NOT NULL DEFAULT '', "piouz" integer NOT NULL DEFAULT '0', "time" integer NOT NULL DEFAULT '0', "soluceId" uuid, CONSTRAINT "REL_67bab4ab2ebc496a545c586b2e" UNIQUE ("soluceId"), CONSTRAINT "PK_d3f1a7a6f09f1c3144bacdc6bcc" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" ADD CONSTRAINT "FK_feb4e8da5bbaf3537c3de6d9b9f" FOREIGN KEY ("levelId") REFERENCES "level"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" ADD CONSTRAINT "FK_0f65a029b9f34234fa83e917c51" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" ADD CONSTRAINT "FK_09cc391a19d7d701da8dc4f07eb" FOREIGN KEY ("soluceOfId") REFERENCES "level"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "news" ADD CONSTRAINT "FK_18ab67e7662dbc5d45dc53a6e00" FOREIGN KEY ("authorId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" ADD CONSTRAINT "FK_762c9a85cfac5e31d8e77abcac3" FOREIGN KEY ("authorId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" ADD CONSTRAINT "FK_67bab4ab2ebc496a545c586b2e1" FOREIGN KEY ("soluceId") REFERENCES "try"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "level" DROP CONSTRAINT "FK_67bab4ab2ebc496a545c586b2e1"`,
    );
    await queryRunner.query(
      `ALTER TABLE "level" DROP CONSTRAINT "FK_762c9a85cfac5e31d8e77abcac3"`,
    );
    await queryRunner.query(
      `ALTER TABLE "news" DROP CONSTRAINT "FK_18ab67e7662dbc5d45dc53a6e00"`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" DROP CONSTRAINT "FK_09cc391a19d7d701da8dc4f07eb"`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" DROP CONSTRAINT "FK_0f65a029b9f34234fa83e917c51"`,
    );
    await queryRunner.query(
      `ALTER TABLE "try" DROP CONSTRAINT "FK_feb4e8da5bbaf3537c3de6d9b9f"`,
    );
    await queryRunner.query(`DROP TABLE "level"`);
    await queryRunner.query(`DROP TABLE "user"`);
    await queryRunner.query(`DROP TABLE "news"`);
    await queryRunner.query(`DROP TABLE "try"`);
  }
}
