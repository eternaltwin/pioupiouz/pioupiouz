import { MigrationInterface, QueryRunner } from "typeorm";
import officialLevels from "./official_levels.json";
import * as crypto from "crypto";
import { decodeLevelData, encodeLevelData } from "piou-core/encoding";

export class AddOfficialLevels1677931920132 implements MigrationInterface {
  name = "AddOfficialLevels1677931920132";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "level"
            ADD "official" boolean NOT NULL`);

    const officialUserUuid = crypto.randomUUID();
    await queryRunner.query(
      `INSERT INTO "user" (id, "displayName")
                                 VALUES ($1, $2)`,
      [officialUserUuid, "PiouPiouZ"],
    );
    for (const level of officialLevels.filter((n) => n.tutorial === "0")) {
      const lvlData = encodeLevelData(decodeLevelData(level.data, true)); // Unapply obfu mappings
      // Published in 2006, with the game release
      await queryRunner.query(
        `INSERT INTO "level" (id, "authorId", "publishedAt", "isPublished", theme, title, description, data, piouz, time, official)
                                     VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`,
        [
          crypto.randomUUID(),
          officialUserUuid,
          new Date(level.cdate),
          true,
          level.theme,
          `${level.pos} - ${level.title}`,
          level.comment || "",
          lvlData,
          level.piouz,
          level.time,
          true,
        ],
      );
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE
                                 FROM "level"
                                 WHERE "official" = true`);
    await queryRunner.query(`ALTER TABLE "level" DROP COLUMN "official"`);
  }
}
