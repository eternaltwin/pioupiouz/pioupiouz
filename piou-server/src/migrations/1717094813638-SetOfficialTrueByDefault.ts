import { MigrationInterface, QueryRunner } from "typeorm";

export class SetOfficialTrueByDefault1717094813638
  implements MigrationInterface
{
  name = "SetOfficialTrueByDefault1717094813638";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "level" ALTER COLUMN "official" SET DEFAULT false`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "level" ALTER COLUMN "official" DROP DEFAULT`,
    );
  }
}
