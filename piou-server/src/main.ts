import { NestApplication, NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { ConfigService } from "@nestjs/config";
import session from "express-session";
import passport from "passport";
import { Request, Response } from "express";
import { join } from "path";

async function bootstrap() {
  const app = await NestFactory.create<NestApplication>(AppModule);

  const configService = app.get(ConfigService);
  const isProduction = configService.getOrThrow<boolean>("production");

  app.enableCors({
    origin: configService.getOrThrow<string>("corsUrl"),
    credentials: true,
  });

  app.use(
    // TODO: Use a production ready session store
    session({
      secret: configService.getOrThrow<string>("session.secret"),
      resave: false,
      saveUninitialized: false,
      cookie: { secure: false }, // TODO: Set to true in production
    }),
  );
  app.use(passport.initialize());
  app.use(passport.session());

  app.useStaticAssets(join(__dirname, "public"));
  app.setBaseViewsDir(join(__dirname, "templates"));
  app.setViewEngine("pug");

  // Inject some vars to templates
  app.use((req: Request, res: Response, next: () => void) => {
    res.locals.query = req.query;
    res.locals.env = {
      dev: !isProduction,
      allowUserRegistration: configService.getOrThrow<boolean>(
        "website.allowRegistration",
      ),
    };
    res.locals.formatDate = (date: Date) => {
      return date.toLocaleString("fr", {
        day: "2-digit",
        month: "short",
        year: "numeric",
        hour: "2-digit",
        minute: "2-digit",
      });
    };

    if (req.user) {
      res.locals.currentUser = req.user;
      res.locals.connected = true;
    }

    next?.();
  });

  // OpenAPI - Swagger
  const config = new DocumentBuilder()
    .setTitle("PiouPiouZ API")
    .setDescription("The PiouPiouZ API description")
    .setVersion("1.0")
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup("swagger", app, document);

  await app.listen(configService.getOrThrow<number>("port"));
}

bootstrap();
