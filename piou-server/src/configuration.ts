export default () => ({
  selfUrl: process.env.API_BASE_URL || "http://localhost:3000/",
  corsUrl: process.env.UI_BASE_URL || "http://localhost:1234/",
  port: +(process.env.PORT || 3000),
  logLevel: process.env.LOG_LEVEL || "info",
  production: process.env.NODE_ENV !== "development",
  database: {
    name: process.env.DATABASE_NAME || "debug",
    host: process.env.DATABASE_HOST || "localhost",
    port: +(process.env.DATABASE_PORT || 5432),
    username: process.env.DATABASE_USERNAME || "user",
    password: process.env.DATABASE_PASSWORD || "pass",
  },
  session: {
    secret: process.env.COOKIE_SECRET || "secretsecretsecretsecretsecretsecret",
  },
  hash: {
    saltRounds: +(process.env.HASH_SALT_ROUNDS || 10), // TODO (not used)
  },
  admin: {
    cookieName: "adminjs",
    cookieSecret: process.env.COOKIE_SECRET || "secret",
    sessionSecret: process.env.COOKIE_SECRET || "secret",
  },
  eternalTwin: {
    url: process.env.ETWIN_URL || "http://localhost:60321/",
    clientId: process.env.ETWIN_CLIENT_ID || "pioupiouz_dev@clients",
    clientSecret: process.env.ETWIN_CLIENT_SECRET || "dev",
    clientScopes: process.env.ETWIN_CLIENT_SCOPES || "base",
  },
  website: {
    allowRegistration: process.env.ALLOW_USER_REGISTRATION !== "false",
  },
});
