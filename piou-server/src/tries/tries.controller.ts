import { Body, Controller, Get, Param, Post, UseGuards } from "@nestjs/common";
import { TriesService } from "./tries.service";
import { CreateTryDto } from "./dto/create-try.dto";
import { ApiTags } from "@nestjs/swagger";
import { PaginateQueryOptions, ReqUser } from "../helpers";
import { User } from "../users/entities/user.entity";
import { Paginate, PaginateQuery } from "nestjs-paginate";
import { AuthenticatedGuard } from "../auth/authenticated.guard";
import { Try } from "./entities/try.entity";
import { UpdateUserFromDbPipe } from "../helpers.pipe";

@ApiTags("tries")
@UseGuards(AuthenticatedGuard)
@Controller("api")
export class TriesController {
  constructor(private readonly triesService: TriesService) {}

  @Get("tries")
  @PaginateQueryOptions(Try, "levelId", "userId")
  findAll(
    @ReqUser(UpdateUserFromDbPipe) user: User,
    @Paginate() query: PaginateQuery,
  ) {
    return this.triesService.findAll(user, query);
  }

  @Get("tries/:id")
  findOne(@Param("id") id: string, @ReqUser(UpdateUserFromDbPipe) user: User) {
    return this.triesService.findOne(user, id);
  }

  @Post("levels/:id/tries")
  createTryFromLevel(
    @Param("id") levelId: string,
    @Body() createOrUpdateDto: CreateTryDto,
    @ReqUser(UpdateUserFromDbPipe) user: User,
  ) {
    return this.triesService.createOrUpdate(user, levelId, createOrUpdateDto);
  }
}
