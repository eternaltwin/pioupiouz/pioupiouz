import { PartialType } from "@nestjs/swagger";
import { CreateTryDto } from "./create-try.dto";

export class UpdateTryDto extends PartialType(CreateTryDto) {}
