import { Module } from "@nestjs/common";
import { TriesService } from "./tries.service";
import { TriesController } from "./tries.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Try } from "./entities/try.entity";
import { UsersModule } from "../users/users.module";

@Module({
  controllers: [TriesController],
  providers: [TriesService],
  imports: [TypeOrmModule.forFeature([Try]), UsersModule],
  exports: [TriesService],
})
export class TriesModule {}
