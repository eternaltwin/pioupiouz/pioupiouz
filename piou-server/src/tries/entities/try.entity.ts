import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm";
import { User } from "../../users/entities/user.entity";
import { Level } from "../../levels/entities/level.entity";

@Entity()
@Unique(["user", "level"])
export class Try extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;
  @UpdateDateColumn()
  createdAt: Date;

  @Column()
  soluce: string;
  @Column()
  time: number;
  @Column()
  piouz: number;

  @ManyToOne(() => Level, (level) => level.tries, { onDelete: "CASCADE" })
  @JoinColumn()
  level: Level;
  @Column()
  levelId: string;
  @ManyToOne(() => User, (user) => user.tries, {
    // TODO: Handle user deletion
    onDelete: "RESTRICT",
  })
  @JoinColumn()
  user: User;
  @Column()
  userId: string;

  @OneToOne(() => Level, (level) => level.soluce, {
    nullable: true,
    onDelete: "CASCADE",
  })
  @JoinColumn()
  soluceOf: Level;
  @Column({ nullable: true })
  soluceOfId: string;
}
