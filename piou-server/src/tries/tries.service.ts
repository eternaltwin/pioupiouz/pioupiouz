import { Injectable, NotFoundException } from "@nestjs/common";
import { CreateTryDto } from "./dto/create-try.dto";
import { FilterOperator, paginate, PaginateQuery } from "nestjs-paginate";
import { InjectRepository } from "@nestjs/typeorm";
import { Try } from "./entities/try.entity";
import { MoreThan, Repository } from "typeorm";
import { User } from "../users/entities/user.entity";
import { isUserAdmin } from "../auth/admin.guard";
import { UsersService } from "../users/users.service";

@Injectable()
export class TriesService {
  constructor(
    @InjectRepository(Try)
    private tryRepository: Repository<Try>,
    private readonly usersService: UsersService,
  ) {}

  async createOrUpdate(
    user: User,
    levelId: string,
    createTryDto: CreateTryDto,
  ) {
    // Check if the try is better than the previous one
    const previousTry = await this.tryRepository.findOne({
      where: { user: { id: user.id }, level: { id: levelId } },
    });
    if (
      previousTry != null &&
      (previousTry.piouz > createTryDto.piouz ||
        (previousTry.piouz >= createTryDto.piouz &&
          previousTry.time < createTryDto.time))
    ) {
      return previousTry;
    }

    let newTry: Try;
    if (previousTry) {
      // Update the existing entity
      newTry = this.tryRepository.merge(previousTry, createTryDto);
    } else {
      // Create a new entity
      newTry = this.tryRepository.create({
        ...createTryDto,
        level: { id: levelId },
        user: user,
      });
    }

    this.usersService.updateUserStats(user);

    return this.tryRepository.save(newTry);
  }

  async findAll(user: User, query: PaginateQuery) {
    return paginate(query, this.tryRepository, {
      sortableColumns: ["createdAt"],
      defaultSortBy: [["createdAt", "DESC"]],
      filterableColumns: {
        userId: [FilterOperator.EQ],
        levelId: [FilterOperator.EQ],
      },
      where: { user: { id: user.id } }, // TODO: Check if the SQL query is correct
    });
  }

  async getBestTries(levelId: string, limit: number = 20) {
    return this.tryRepository.find({
      where: { level: { id: levelId }, piouz: MoreThan(0) },
      // from the  largest number of piouz  saved to the smallest, and  from the
      // smallest time to the largest time, and from oldest to newest.
      order: { piouz: "DESC", time: "ASC", createdAt: "ASC" },
      take: limit,
      relations: ["user"],
    });
  }

  async findOne(user: User, id: string) {
    const tryy = await this.tryRepository.findOneBy({ id });
    if (!tryy) throw new NotFoundException("Try not found");
    if (tryy.userId !== user.id && !isUserAdmin(user)) {
      throw new NotFoundException("Try not found");
    }
    return tryy;
  }

  async findOneOf(user: User, levelId: string) {
    return await this.tryRepository.findOneBy({
      user: { id: user.id },
      level: { id: levelId },
    });
  }

  async deleteAllTries(levelId: string) {
    await this.tryRepository.delete({ level: { id: levelId } });
  }
}
