export class UpdateLevelDto {
  title?: string;
  description?: string;
  theme?: number;

  data?: string;
  piouz?: number;
  time?: number;
}
