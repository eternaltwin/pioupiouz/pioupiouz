export class CreateLevelDto {
  title: string;
  description?: string;
  theme?: number;
}
