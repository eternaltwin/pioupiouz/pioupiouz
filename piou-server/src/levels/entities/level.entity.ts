import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { User } from "../../users/entities/user.entity";
import { Try } from "../../tries/entities/try.entity";

@Entity()
export class Level extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;
  @ManyToOne(() => User, (user) => user.levels, {
    // TODO: Handle user deletion
    onDelete: "RESTRICT",
  })
  @JoinColumn()
  author: User;
  @Column()
  authorId: string;
  @Column({ default: false })
  official: boolean;

  @Column({ default: false })
  isPublished: boolean;
  @Column({ nullable: true })
  publishedAt: Date;

  @Column("smallint", { default: 0 })
  theme: number;
  @Column({ length: 255 })
  title: string;
  @Column({ default: "" })
  description: string;
  @Column({ default: "" })
  data: string;
  @Column({ default: 0 })
  piouz: number;
  @Column({ default: 0 })
  time: number;

  @OneToOne(() => Try, (tryy) => tryy.level, {
    nullable: true,
    onDelete: "SET NULL",
  })
  @JoinColumn()
  soluce: Try;
  @Column({ nullable: true })
  soluceId: string;

  @OneToMany(() => Try, (tryy) => tryy.level)
  tries: Try[];
}
