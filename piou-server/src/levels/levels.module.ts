import { Module } from "@nestjs/common";
import { LevelsService } from "./levels.service";
import { LevelsController } from "./levels.controller";
import { Level } from "./entities/level.entity";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "../users/entities/user.entity";
import { Try } from "../tries/entities/try.entity";
import { TriesModule } from "../tries/tries.module";
import { UsersModule } from "../users/users.module";

@Module({
  controllers: [LevelsController],
  providers: [LevelsService],
  imports: [
    TypeOrmModule.forFeature([Level, User, Try]),
    TriesModule,
    UsersModule,
  ],
  exports: [LevelsService],
})
export class LevelsModule {}
