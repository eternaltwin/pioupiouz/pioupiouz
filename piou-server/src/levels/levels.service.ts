import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from "@nestjs/common";
import { CreateLevelDto } from "./dto/create-level.dto";
import { UpdateLevelDto } from "./dto/update-level.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Level } from "./entities/level.entity";
import { FindOptionsWhere, Repository } from "typeorm";
import {
  FilterOperator,
  paginate,
  Paginated,
  PaginateQuery,
} from "nestjs-paginate";
import { User } from "../users/entities/user.entity";
import { TriesService } from "../tries/tries.service";
import { decodeLevelData } from "piou-core/encoding";

@Injectable()
export class LevelsService {
  constructor(
    @InjectRepository(Level)
    private readonly levelRepository: Repository<Level>,
    private readonly triesService: TriesService,
  ) {}

  async create(user: User, createLevelDto: CreateLevelDto) {
    return this.levelRepository.save(
      Level.create({
        ...createLevelDto,
        author: user,
      }),
    );
  }

  async findAll(
    query: PaginateQuery,
    isPublished: boolean,
  ): Promise<Paginated<Level>> {
    const whereConfig: FindOptionsWhere<Level> = {};
    whereConfig.isPublished = isPublished;

    return paginate(query, this.levelRepository, {
      sortableColumns: ["publishedAt"],
      defaultSortBy: [["publishedAt", "DESC"]],
      searchableColumns: ["title", "description"],
      filterableColumns: {
        authorId: [FilterOperator.EQ],
      },
      where: whereConfig,
      relations: ["author"],
    });
  }

  async findOne(user: User, id: string): Promise<Level> {
    const level = await this.levelRepository.findOne({
      where: { id },
      relations: ["author"],
    });
    if (!level) throw new NotFoundException();
    if (!level.isPublished && level.authorId !== user.id) {
      throw new NotFoundException();
    }
    return level;
  }

  async update(id: string, updateLevelDto: UpdateLevelDto) {
    if (updateLevelDto.data !== undefined) {
      await this.triesService.deleteAllTries(id);
      updateLevelDto.theme = decodeLevelData(updateLevelDto.data).did;
    }
    return this.levelRepository.update(id, updateLevelDto);
  }

  async publish(user: User, id: string, newPiouz: number) {
    const level = await this.findOne(user, id);
    if (level.isPublished)
      throw new BadRequestException(
        "You can't publish an already published level",
      );
    if (user.id !== level.authorId)
      throw new BadRequestException("You can't publish someone else's level");

    const bestTry = await this.triesService.findOneOf(user, level.id);
    if (!bestTry)
      throw new BadRequestException(
        "You must complete the level before publishing it",
      );

    if ((newPiouz ?? level.piouz) > bestTry.piouz)
      throw new BadRequestException(
        "You can't publish a level with more piouz than your best try",
      );

    level.isPublished = true;
    level.publishedAt = new Date();
    return this.levelRepository.save(level);
  }

  async remove(user: User, id: string) {
    const level = await this.findOne(user, id);
    if (level.isPublished) {
      throw new BadRequestException("You can't delete a published level");
    }
    return this.levelRepository.remove(level);
  }

  async getHighScores(id: string) {
    return this.triesService.getBestTries(id);
  }
}
