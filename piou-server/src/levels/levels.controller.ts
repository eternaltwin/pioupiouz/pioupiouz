import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from "@nestjs/common";
import { LevelsService } from "./levels.service";
import { CreateLevelDto } from "./dto/create-level.dto";
import { UpdateLevelDto } from "./dto/update-level.dto";
import { ApiTags } from "@nestjs/swagger";
import { Paginate, PaginateQuery } from "nestjs-paginate";
import { PaginateQueryOptions, ReqUser } from "../helpers";
import { User } from "../users/entities/user.entity";
import { AuthenticatedGuard } from "../auth/authenticated.guard";
import { PublishLevelDto } from "./dto/publish-level.dto";
import { Level } from "./entities/level.entity";
import { UpdateUserFromDbPipe } from "../helpers.pipe";

@ApiTags("levels")
@UseGuards(AuthenticatedGuard)
@Controller("api/levels")
export class LevelsController {
  constructor(private readonly levelsService: LevelsService) {}

  @Post()
  create(
    @Body() createLevelDto: CreateLevelDto,
    @ReqUser(UpdateUserFromDbPipe) user: User,
  ) {
    return this.levelsService.create(user, createLevelDto);
  }

  @Get()
  @PaginateQueryOptions(Level, "authorId")
  findAll(@Paginate() query: PaginateQuery) {
    return this.levelsService.findAll(query, true);
  }

  @Get(":id")
  findOne(@Param("id") id: string, @ReqUser(UpdateUserFromDbPipe) user: User) {
    return this.levelsService.findOne(user, id);
  }

  @Patch(":id")
  update(@Param("id") id: string, @Body() updateLevelDto: UpdateLevelDto) {
    return this.levelsService.update(id, updateLevelDto);
  }

  @Post(":id/publish")
  publish(
    @Param("id") id: string,
    @ReqUser(UpdateUserFromDbPipe) user: User,
    @Body() { piouz }: PublishLevelDto,
  ) {
    return this.levelsService.publish(user, id, piouz);
  }

  @Delete(":id")
  remove(@Param("id") id: string, @ReqUser(UpdateUserFromDbPipe) user: User) {
    return this.levelsService.remove(user, id);
  }
}
