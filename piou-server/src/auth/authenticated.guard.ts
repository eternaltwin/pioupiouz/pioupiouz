import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Request as ExpressRequest } from "express";

@Injectable()
export class AuthenticatedGuard implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest<ExpressRequest>();
    return request.isAuthenticated();
  }
}
