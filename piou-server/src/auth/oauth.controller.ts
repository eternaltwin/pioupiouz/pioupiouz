import {
  Controller,
  Get,
  HttpRedirectResponse,
  Inject,
  Query,
  Redirect,
  Req,
  UseGuards,
  BadRequestException,
  InternalServerErrorException,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { AuthenticatedGuard } from "./authenticated.guard";
import { ConfigService } from "@nestjs/config";
import { ReqUser } from "../helpers";
import { SessionUser } from "../users/entities/user.entity";
import { Request as ExpressRequest } from "express";
import { UsersService } from "../users/users.service";
import { AuthService } from "./auth.service";

// @ts-expect-error ES Module import (type only)
import type { RfcOauthClient } from "@eternaltwin/oauth-client-http/rfc-oauth-client";
// @ts-expect-error ES Module import (type only)
import type { EternaltwinNodeClient } from "@eternaltwin/client-node";

@ApiTags("oauth")
@Controller("oauth")
export class OAuthController {
  constructor(
    private readonly configService: ConfigService,
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    @Inject("ETERNALTWIN_OAUTH_CLIENT")
    private readonly oauthClient: RfcOauthClient,
    @Inject("ETERNALTWIN_NODE_CLIENT")
    private readonly nodeClient: EternaltwinNodeClient,
  ) {}

  @Get("/login")
  @Redirect("/")
  async initOAuth() {
    const response: HttpRedirectResponse = {
      url: this.oauthClient
        .getAuthorizationUri(
          this.configService.getOrThrow<string>("eternalTwin.clientScopes"),
          "",
        )
        .toString(),
      statusCode: 302,
    };

    return response;
  }

  @Get("/signup")
  @Redirect("/")
  async register() {
    return {
      url: this.configService.getOrThrow("eternalTwin.url") + "register",
      statusCode: 302,
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Get("/link")
  @Redirect("/")
  async link(@ReqUser() sessionUser: SessionUser) {
    return this.oauthClient
      .getAuthorizationUri(
        this.configService.getOrThrow<string>("eternalTwin.clientScopes"),
        sessionUser.id,
      )
      .toString();
  }

  @Get("/callback")
  @Redirect("/")
  async callback(
    @Query("code") code: string,
    @Query("state") state: string,
    @Req() req: ExpressRequest,
    @ReqUser() sessionUser?: SessionUser,
  ) {
    const token = await this.oauthClient.getAccessToken(code);

    const self = await this.nodeClient.getAuthSelf({ auth: token.accessToken });

    if (self.type !== /* TODO AuthType.AccessToken */ 3) {
      throw new BadRequestException("Invalid auth type");
    }

    const { user: etwinUser } = self;

    // Verify that the EternalTwin account is not already linked to a user
    const existingUser = await this.usersService.findOne({
      eternalTwinId: etwinUser.id,
    });

    // If there is a state, it's a link request
    if (state !== "") {
      if (sessionUser?.id !== state) {
        throw new BadRequestException("?????????????");
      }

      const user = await this.usersService.findOne({ id: state });
      if (user == null) {
        throw new BadRequestException("Invalid user id in state");
      }

      if (existingUser) {
        if (existingUser.id === user.id) {
          throw new BadRequestException(
            "EternalTwin account already linked to this user",
          );
        }

        throw new BadRequestException(
          "EternalTwin account already linked to another user. Please contact an administrator to unlink.",
        );
      }

      await this.usersService.associateEternalTwinId(user, etwinUser.id);
      return;
    }

    // Here, it's not a link request.

    // Check if user is not already logged in
    if (sessionUser) {
      return { url: "/", statusCode: 302 };
    }

    // If the user with the EternalTwin ID exists, log in as that user
    if (existingUser) {
      if (!(await this.authService.login(req, existingUser))) {
        throw new InternalServerErrorException("Failed to log in");
      }

      return;
    }

    // If the user is not logged in, create a new user
    const newUser = await this.usersService.createWithEternalTwin(etwinUser);

    if (!(await this.authService.login(req, newUser))) {
      throw new InternalServerErrorException("Failed to log in");
    }

    return;
  }
}
