import {
  Controller,
  Get,
  Post,
  Redirect,
  Request,
  UseGuards,
} from "@nestjs/common";
import { ApiBody, ApiProperty, ApiTags } from "@nestjs/swagger";
import { Request as ExpressRequest } from "express";
import { AuthenticatedGuard } from "./authenticated.guard";
import { User } from "../users/entities/user.entity";
import { LocalAuthGuard } from "./local-auth.guard";

class LoginDto {
  @ApiProperty()
  username: string;
  @ApiProperty()
  password: string;
}

@ApiTags("auth")
@Controller("auth")
export class AuthController {
  @UseGuards(LocalAuthGuard)
  @Post("login")
  @ApiBody({ type: LoginDto })
  async login(@Request() req: ExpressRequest) {
    // Throw 401 if login failed
    return req.user as User;
  }

  @Get("logout")
  @Redirect("/")
  @UseGuards(AuthenticatedGuard)
  async logout(@Request() req: ExpressRequest) {
    req.session.destroy(() => {});
  }
}
