import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import {SessionUser} from "../users/entities/user.entity";
import {Request as ExpressRequest} from "express";

export function isUserAdmin(user: SessionUser) {
  return user.isAdmin;
}

@Injectable()
export class AdminGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const { user } = context.switchToHttp().getRequest<ExpressRequest>() as { user?: SessionUser }
    if (!user)
        return false;

    return isUserAdmin(user);
  }
}
