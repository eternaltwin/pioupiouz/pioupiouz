import { Module } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { LocalStrategy } from "./local.strategy";
import { UsersModule } from "../users/users.module";
import { PassportModule } from "@nestjs/passport";
import { SessionSerializer } from "./session.serializer";
import { AuthController } from "./auth.controller";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { OAuthController } from "./oauth.controller";

@Module({
  providers: [
    AuthService,
    LocalStrategy,
    ConfigService,
    SessionSerializer,
    {
      provide: "ETERNALTWIN_OAUTH_CLIENT",
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const { RfcOauthClient } = await import(
          "@eternaltwin/oauth-client-http/rfc-oauth-client"
        );
        const { Url } = await import("@eternaltwin/core/core/url");

        const etwinUrl = configService.getOrThrow<string>("eternalTwin.url");
        const selfUrl = configService.getOrThrow<string>("selfUrl");

        return new RfcOauthClient({
          authorizationEndpoint: new Url(`${etwinUrl}oauth/authorize`),
          tokenEndpoint: new Url(`${etwinUrl}oauth/token`),
          callbackEndpoint: new Url(`${selfUrl}oauth/callback`),
          clientId: configService.getOrThrow<string>("eternalTwin.clientId"),
          clientSecret: configService.getOrThrow<string>(
            "eternalTwin.clientSecret",
          ),
        });
      },
    },
    {
      provide: "ETERNALTWIN_NODE_CLIENT",
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const { EternaltwinNodeClient } = await import(
          "@eternaltwin/client-node"
        );
        const { Url } = await import("@eternaltwin/core/core/url");

        return new EternaltwinNodeClient(
          new Url(configService.getOrThrow<string>("eternalTwin.url")),
        );
      },
    },
  ],
  imports: [
    UsersModule,
    ConfigModule,
    PassportModule.register({ session: true }),
  ],
  exports: [AuthService],
  controllers: [AuthController, OAuthController],
})
export class AuthModule {}
