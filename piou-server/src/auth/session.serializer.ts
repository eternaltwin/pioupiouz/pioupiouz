import { Injectable } from "@nestjs/common";
import { PassportSerializer } from "@nestjs/passport";
import { SessionUser, User } from "../users/entities/user.entity";

@Injectable()
export class SessionSerializer extends PassportSerializer {
  serializeUser(
    user: User,
    done: (err: Error | null, user: SessionUser) => void,
  ): void {
    done(null, {
      id: user.id,
      userName: user.userName,
      displayName: user.displayName,
      eternalTwinId: user.eternalTwinId,
      isAdmin: user.isAdmin,
    });
  }

  deserializeUser(
    payload: SessionUser,
    done: (err: Error | null, payload?: SessionUser) => void,
  ): void {
    done(null, payload);
  }
}
