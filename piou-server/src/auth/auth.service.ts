import { Injectable, Logger } from "@nestjs/common";
import { UsersService } from "../users/users.service";
import { User } from "../users/entities/user.entity";
import { Request as ExpressRequest } from "express";
import * as bcrypt from "bcrypt";

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService) {}

  async validateUser(username: string, pass: string): Promise<User | null> {
    const user = await this.usersService.findOne({ username: username });
    if (user && (await bcrypt.compare(pass, user.password))) {
      return user;
    }
    return null;
  }

  async login(request: ExpressRequest, user: User): Promise<boolean> {
    return new Promise((resolve, reject) => {
      request.login(user, (err) => {
        if (err) {
          Logger.warn(err);
          reject(false);
        }
        resolve(true);
      });
    });
  }
}
