import {
  BadRequestException,
  Body,
  Controller,
  Logger,
  NotFoundException,
  NotImplementedException,
  Param,
  Post,
  Redirect,
  Req,
  UnauthorizedException,
  UseGuards,
} from "@nestjs/common";
import { Request as ExpressRequest } from "express";
import { ApiTags } from "@nestjs/swagger";
import { UsersService } from "../users/users.service";
import { TriesService } from "../tries/tries.service";
import { LevelsService } from "../levels/levels.service";
import { AuthService } from "../auth/auth.service";
import { AuthenticatedGuard } from "../auth/authenticated.guard";
import { ReqUser } from "../helpers";
import { User } from "../users/entities/user.entity";
import { decodeExitGame } from "piou-core/encoding";
import { Try } from "../tries/entities/try.entity";
import { ConfigService } from "@nestjs/config";
import { UpdateUserFromDbPipe } from "../helpers.pipe";

@ApiTags("views")
@Controller()
export class ViewsPostController {
  constructor(
    private readonly usersService: UsersService,
    private readonly levelsService: LevelsService,
    private readonly triesService: TriesService,
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {}

  //region Auth
  @Post("login")
  @Redirect("/")
  async loginHandler(
    @Body("username") username: string,
    @Body("password") password: string,
    @Req() req: ExpressRequest,
  ) {
    // TODO: make a better login system
    Logger.log("LOGIN", username);
    const user = await this.authService.validateUser(username, password);
    if (!user) return { url: "/login?error=Login%20ou%20password%20incorrect" };

    // login promise
    await new Promise((resolve, reject) => {
      req.login(user, (err) => {
        if (err) {
          reject(err);
        }
        resolve(user);
      });
    });
    return;
  }

  @UseGuards(AuthenticatedGuard)
  @Post("changePassword")
  @Redirect("/")
  async changePassword(
    @ReqUser(UpdateUserFromDbPipe) user: User,
    @Body("password") password: string,
    @Body("passwordConfirm") passwordConfirm: string,
  ) {
    if (password !== passwordConfirm)
      return {
        url: `/user/${user.id}?error=La%20confirmation%20du%20mot%20de%20passe%20ne%20correspond%20pas`,
      };

    // TODO: Update password !!
    throw new NotImplementedException("Update password not implemented");

    // return { url: `/user/${user.id}` };
  }

  @Post("signup")
  @Redirect("/")
  async signupHandler(
    @Req() req: ExpressRequest,
    @Body("username") username: string,
    @Body("password") password: string,
    @Body("passwordConfirm") passwordConfirm: string,
  ) {
    if (!this.configService.getOrThrow<boolean>("website.allowRegistration"))
      throw new UnauthorizedException("Registration is disabled");

    if (!username) return { url: "/signup?fail=errLoginMissing" };
    if (!password) return { url: "/signup?fail=errPasswordMissing" };
    if (password !== passwordConfirm)
      return { url: "/auth/signup?fail=errPassConf" };
    try {
      Logger.log("SIGNUP", username);
      let user = await this.usersService.create(username, password);
      await this.authService.login(req, user);
    } catch (e) {
      Logger.log("Error while creating user", e);
      // Unique constraint violation
      if (e.code === "P2002") {
        return { url: "/auth/signup?fail=errLoginExists" };
      } else {
        return { url: "/auth/signup?fail=errUnknown" };
      }
    }
  }

  //endregion

  //region levels
  @UseGuards(AuthenticatedGuard)
  @Post("level/create")
  @Redirect()
  async createLevelRoute(
    @ReqUser(UpdateUserFromDbPipe) user: User,
    @Body("title") title: string,
    @Body("description") description?: string,
  ) {
    const level = await this.levelsService.create(user, { title, description });

    return { url: `/level/${level.id}/edit` };
  }

  @UseGuards(AuthenticatedGuard)
  @Post("level/:id/edit")
  @Redirect()
  async editLevelRoute(
    @Param("id") levelId: string,
    @ReqUser(UpdateUserFromDbPipe) user: User,
    @Body("title") title?: string,
    @Body("description") description?: string,
    @Body("piouz") piouz?: number,
  ) {
    const level = await this.levelsService.findOne(user, levelId);
    if (!level) throw new NotFoundException();
    if (level.authorId !== user.id) throw new UnauthorizedException();
    if (level.isPublished)
      throw new UnauthorizedException("Level is already published");
    if (piouz === 0) throw new UnauthorizedException("Can't set piouz to 0");

    // Ensure author best try is superior or equal to piouz
    if (piouz !== undefined) {
      const bestTry = await this.triesService.findOneOf(user, levelId);
      if (!bestTry) throw new UnauthorizedException();
      if (bestTry.piouz < piouz) throw new UnauthorizedException();
    }

    await this.levelsService.update(levelId, { title, description, piouz });

    return { url: `/level/${levelId}/edit` };
  }

  @UseGuards(AuthenticatedGuard)
  @Post("level/:id/publish")
  @Redirect()
  async publishLevelRoute(
    @Param("id") levelId: string,
    @ReqUser(UpdateUserFromDbPipe) user: User,
    @Body("piouz") newPiouz?: number,
  ) {
    const level = await this.levelsService.findOne(user, levelId);
    if (!level) throw new NotFoundException();
    if (level.isPublished) throw new UnauthorizedException();
    if (level.authorId !== user.id) throw new UnauthorizedException();

    const bestTry = await this.triesService.findOneOf(user, levelId);
    if (!bestTry) throw new UnauthorizedException("No try");

    if ((newPiouz ?? level.piouz) > bestTry.piouz)
      throw new UnauthorizedException("Level piouz is superior to best try");

    await this.levelsService.publish(user, levelId, newPiouz ?? level.piouz);

    return { url: `/level/${levelId}` };
  }

  @UseGuards(AuthenticatedGuard)
  @Post("/level/:id/save")
  @Redirect()
  async saveLevelRoute(
    @Param("id") levelId: string,
    @Body("$data") data: string,
    @ReqUser(UpdateUserFromDbPipe) user: User,
  ) {
    const level = await this.levelsService.findOne(user, levelId);
    if (!level) throw new NotFoundException();
    if (level.authorId !== user.id) throw new UnauthorizedException();
    if (level.isPublished) throw new UnauthorizedException();

    await this.levelsService.update(level.id, { data: data });

    return { url: `/level/${levelId}/edit` };
  }

  @UseGuards(AuthenticatedGuard)
  @Post("/level/:id/delete")
  @Redirect("/level/create")
  async deleteLevelRoute(
    @Param("id") levelId: string,
    @ReqUser(UpdateUserFromDbPipe) user: User,
  ) {
    const level = await this.levelsService.findOne(user, levelId);
    if (!level) throw new NotFoundException();
    if (level.authorId !== user.id) throw new UnauthorizedException();
    if (level.isPublished) throw new UnauthorizedException();

    await this.levelsService.remove(user, levelId);
  }

  /**
   * Endpoint called by the game on finish.
   * `request.body.$data` contains the `ExitGameData` object
   * The game will redirect the user to the `url.substring(4)`.
   *
   * This request is also sent when the player replay the level.
   */
  @Post("/level/:id/end")
  async endLevelRoute(
    @Param("id") id: string,
    @Body("$data") exitData: string,
    @ReqUser(UpdateUserFromDbPipe) user: User,
  ) {
    if (!user) return "1324/login";

    const level = await this.levelsService.findOne(user, id);
    if (!level) throw new NotFoundException();
    if (!level.isPublished && level.authorId !== user.id)
      throw new UnauthorizedException();

    const exitGame = decodeExitGame(exitData);
    if (!exitData) throw new BadRequestException();
    if (!exitGame.$soluce) return "1234/error?error=noSoluce";

    let bestTry: Try;
    try {
      bestTry = await this.triesService.createOrUpdate(user, level.id, {
        piouz: exitGame.$piouz,
        soluce: exitGame.$soluce,
        time: exitGame.$time,
      });
    } catch (e) {
      console.error(e);
      return "1234/error?error=" + e.constructor.name;
    }

    if (!level.isPublished) {
      if (bestTry.piouz > 0) {
        return "1234/level/" + id + "/publish";
      }
      return "1234/level/" + id + "/edit";
    }

    return "1234/level/" + id;
  }

  //endregion
}
