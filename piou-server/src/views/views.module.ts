import { Module } from "@nestjs/common";
import { ViewsController } from "./views.controller";
import { UsersModule } from "../users/users.module";
import { LevelsModule } from "../levels/levels.module";
import { TriesModule } from "../tries/tries.module";
import { NewsModule } from "../news/news.module";
import { AuthModule } from "../auth/auth.module";
import { ViewsPostController } from "./views-post.controller";
import { ConfigModule } from "@nestjs/config";

@Module({
  controllers: [ViewsController, ViewsPostController],
  imports: [
    UsersModule,
    LevelsModule,
    TriesModule,
    NewsModule,
    AuthModule,
    ConfigModule,
  ],
})
export class ViewsModule {}
