import {
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Query,
  Redirect,
  Render,
  Req,
  UnauthorizedException,
  UseGuards,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { UsersService } from "../users/users.service";
import { TriesService } from "../tries/tries.service";
import { LevelsService } from "../levels/levels.service";
import { NewsService } from "../news/news.service";
import { ReqUser } from "../helpers";
import { Paginate, PaginateQuery } from "nestjs-paginate";
import { User } from "../users/entities/user.entity";
import { Try } from "../tries/entities/try.entity";
import { AuthenticatedGuard } from "../auth/authenticated.guard";
import { Request as ExpressRequest } from "express";
import { UpdateUserFromDbPipe } from "../helpers.pipe";

@ApiTags("views")
@Controller()
export class ViewsController {
  constructor(
    private readonly usersService: UsersService,
    private readonly levelsService: LevelsService,
    private readonly triesService: TriesService,
    private readonly newsService: NewsService,
  ) {}

  @Get("")
  @Render("static/home")
  async home(
    @Paginate() query: PaginateQuery,
    @ReqUser(UpdateUserFromDbPipe) currentUser?: User,
  ) {
    if (currentUser) {
      const news = await this.newsService.findAll(query);
      return {
        currentUser,
        news: news.data,
        page: news.meta.currentPage,
        nbPages: news.meta.totalPages,
      };
    }
    return {};
  }

  //region Auth
  @Get("login")
  @Render("auth/login")
  async login(@Query("fail") fail: string) {
    return {
      fail,
    };
  }

  @Get("signup")
  @Render("auth/signup")
  async signUp(@Query("fail") fail: string) {
    return {
      fail,
    };
  }

  @Get("/logout")
  @Redirect("/")
  async logOut(@Req() req: ExpressRequest) {
    req.session.destroy(() => {});
  }

  //endregion

  //region try stuff
  @Get("try")
  @Render("static/try")
  async try() {
    return { userCount: "TODO" };
  }

  @Get("try/:id")
  @Render("static/trylevel")
  async tryLevel(@Param("id") id: string, @Query("name") name: string) {
    return { xmlId: id, xmlName: name };
  }

  @Post("try/end")
  async tryLevelEnd() {
    return "1234/try";
  }

  //endregion try

  //region users
  @Get("user")
  @Render("users")
  async users(
    @Paginate() query: PaginateQuery,
    @ReqUser(UpdateUserFromDbPipe) currentUser?: User,
  ) {
    const users = await this.usersService.findAll(query);
    return {
      currentUser,
      users: users.data,
      page: users.meta.currentPage,
      nbPages: users.meta.totalPages,
    };
  }

  @Get("user/:id")
  @Render("user")
  async userPage(
    @Param("id") userId: string,
    @Paginate() query: PaginateQuery,
    @ReqUser(UpdateUserFromDbPipe) currentUser?: User,
  ) {
    const user = await this.usersService.findOne({ id: userId });
    if (!user) throw new NotFoundException();
    await this.usersService.updateUserStats(user);
    query.filter = { authorId: user.id };
    const levels = await this.levelsService.findAll(query, true);

    return {
      currentUser,
      user,
      levels: levels.data,
      page: levels.meta.currentPage,
      nbPages: levels.meta.totalPages,
    };
  }

  //endregion users

  //region levels
  @Get("play")
  @Redirect("/level")
  async level() {}

  @Get("level")
  @Render("levels")
  async levelsPage(
    @Paginate() query: PaginateQuery,
    @ReqUser(UpdateUserFromDbPipe) currentUser?: User,
  ) {
    const levels = await this.levelsService.findAll(query, true);
    return {
      currentUser,
      levels: levels.data,
      page: levels.meta.currentPage,
      nbPages: levels.meta.totalPages,
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Get("level/create")
  @Render("create")
  async createPage(
    @ReqUser(UpdateUserFromDbPipe) user: User,
    @Paginate() query: PaginateQuery,
    @ReqUser(UpdateUserFromDbPipe) currentUser?: User,
  ) {
    query.filter = { authorId: user.id };
    const levels = await this.levelsService.findAll(query, false);
    return {
      currentUser,
      levels: levels.data,
      page: levels.meta.currentPage,
      nbPages: levels.meta.totalPages,
    };
  }

  @Get("level/:id")
  @Render("level")
  async levelPreviewPage(
    @Param("id") levelId: string,
    @ReqUser(UpdateUserFromDbPipe) currentUser: User,
  ) {
    const level = await this.levelsService.findOne(currentUser, levelId);
    const bestTry = currentUser
      ? await this.triesService.findOneOf(currentUser, levelId)
      : null;
    const scores: Try[] = await this.levelsService.getHighScores(levelId);
    return {
      currentUser,
      level,
      bestTry,
      scores,
    };
  }

  @Get("level/:id/play")
  @Render("levelPlay")
  async levelPage(
    @Param("id") levelId: string,
    @ReqUser(UpdateUserFromDbPipe) currentUser: User,
  ) {
    const level = await this.levelsService.findOne(currentUser, levelId);
    return {
      currentUser,
      level,
      bestTry: currentUser
        ? await this.triesService.findOneOf(currentUser, level.id)
        : undefined,
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Get("level/:id/edit")
  @Render("editor")
  async editorPage(
    @Param("id") levelId: string,
    @ReqUser(UpdateUserFromDbPipe) currentUser: User,
  ) {
    const level = await this.levelsService.findOne(currentUser, levelId);
    if (level.authorId !== currentUser.id) throw new UnauthorizedException();
    if (level.isPublished)
      throw new UnauthorizedException("Level is alreay published");

    return {
      currentUser,
      level,
      bestTry: await this.triesService.findOneOf(currentUser, level.id),
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Get("level/:id/publish")
  @Render("publish")
  async publishPage(
    @Param("id") levelId: string,
    @ReqUser(UpdateUserFromDbPipe) currentUser: User,
  ) {
    const level = await this.levelsService.findOne(currentUser, levelId);
    if (level.authorId !== currentUser.id) throw new UnauthorizedException();
    if (level.isPublished)
      throw new UnauthorizedException("Level is already published");

    const bestTry = await this.triesService.findOneOf(currentUser, level.id);
    if (!bestTry) throw new UnauthorizedException("No try found");

    return {
      currentUser,
      level,
      bestTry,
    };
  }

  //endregion levels
}
