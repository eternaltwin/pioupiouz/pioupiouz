import { Injectable } from "@nestjs/common";
import { paginate, PaginateQuery } from "nestjs-paginate";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { News } from "./entities/news.entity";

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News) private newsRepository: Repository<News>,
  ) {}

  findAll(query: PaginateQuery) {
    return paginate(query, this.newsRepository, {
      sortableColumns: ["createdAt"],
      defaultSortBy: [["createdAt", "DESC"]],
      searchableColumns: ["title", "content"],
    });
  }

  findOne(id: string) {
    return this.newsRepository.findOneBy({ id });
  }
}
