import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { User } from "../../users/entities/user.entity";

@Entity()
export class News extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @CreateDateColumn()
  createdAt: Date;

  @Column()
  title: string;
  @Column({ default: "/gfx/news/news_img.jpg" })
  img: string;
  @Column()
  content: string;

  @ManyToOne(() => User, (user) => user.news, {
    // TODO: Handle user deletion
    onDelete: "RESTRICT",
  })
  @JoinColumn()
  author: User;
  @Column()
  authorId: string;
}
