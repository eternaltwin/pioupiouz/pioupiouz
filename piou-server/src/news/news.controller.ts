import { Controller, Get, Param } from "@nestjs/common";
import { NewsService } from "./news.service";
import { ApiTags } from "@nestjs/swagger";
import { PaginateQueryOptions } from "../helpers";
import { Paginate, PaginateQuery } from "nestjs-paginate";
import { News } from "./entities/news.entity";

@ApiTags("news")
@Controller("api/news")
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Get()
  @PaginateQueryOptions(News)
  findAll(@Paginate() query: PaginateQuery) {
    return this.newsService.findAll(query);
  }

  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.newsService.findOne(id);
  }
}
