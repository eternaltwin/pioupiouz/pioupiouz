import { PiouPiouZFlashVars, PlayMode } from "piou-core/ts/types";
import { FlashObject } from "./flashobject";

function createFlashClient(
  swfUrl: string,
  fv: PiouPiouZFlashVars,
  width: number,
  height: number,
  parentId: string
): void {
  let swf = new FlashObject(swfUrl, "play", width, height, 8, "FFFFFF");

  let k: keyof PiouPiouZFlashVars;
  for (k in fv) {
    swf.addVariable(k, fv[k]);
  }
  swf.addParam("allowScriptAccess", "always");
  swf.write(parentId);
}

export function createPlaySwf(
  levelId: string,
  useFlashClient: boolean = false,
  help: boolean = false,
  lang: "en" | "fr" | "es" = "fr"
) {
  const width = 622;
  const height = 424;
  const fv: PiouPiouZFlashVars = {
    swf: "/swf/play.swf",
    $lang: lang,
    $loadUrl: "/xml/level/" + levelId + "?" + Date.now(),
    $endUrl: "/level/" + levelId + "/end",
    $help: help ? 1 : 0,
    $mode: PlayMode.Normal,
  };

  createFlashClient("/swf/loader.swf", fv, width, height, "game_container");
}

export function createPreviewSwf(
  levelId: string,
  useFlashClient: boolean = false,
  big: boolean = false,
  lang: "en" | "fr" | "es" = "fr"
) {
  const width = big ? 232 : 100;
  const height = big ? 232 : 100;
  const fv: PiouPiouZFlashVars = {
    swf: "/swf/play.swf",
    $lang: lang,
    $loadUrl: "/xml/level/" + levelId + "/preview?" + Date.now(),
    $mode: PlayMode.Preview,
  };

    createFlashClient(
      "/swf/loader_preview.swf",
      fv,
      width,
      height,
      "preview-" + levelId
    );
}

export function createReplaySwf(
  levelId: string,
  useFlashClient: boolean = false,
  help: boolean = false,
  lang: "en" | "fr" | "es" = "fr"
) {
  const width = 622;
  const height = 424;
  const fv: PiouPiouZFlashVars = {
    swf: "/swf/play.swf",
    $lang: lang,
    $loadUrl: "/xml/history/" + levelId + "?" + Date.now(),
    $help: help ? 1 : 0,
    $mode: PlayMode.Replay,
  };

    createFlashClient("/swf/loader.swf", fv, width, height, "game_container");

}

export function createEditorSwf(
  levelId: string,
  useFlashClient: boolean = false,
  help: boolean = false,
  lang: "en" | "fr" | "es" = "fr"
) {
  const width = 622;
  const height = 564;
  const fv: PiouPiouZFlashVars = {
    swf: "/swf/edit.swf",
    $lang: lang,
    $loadUrl: "/xml/level/" + levelId + "/edit?" + Date.now(),
    $saveUrl: "/level/" + levelId + "/save",
    $exitUrl: "/level/" + levelId,
    $selfUrl: "TODO",
    $saveAct: "unused",
    $testUrl: "/level/" + levelId + "/play",
    $help: help ? 1 : 0,
    $mode: PlayMode.Edit,
  };

    createFlashClient("/swf/loader.swf", fv, width, height, "game_container");

}
