function getFlashVersion() {
  // ie
  try {
    try {
      // @ts-ignore
      var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
      try {
        axo.AllowScriptAccess = "always";
      } catch (e) {
        return "6,0,0";
      }
    } catch (e) {}
    // @ts-ignore
    return new ActiveXObject("ShockwaveFlash.ShockwaveFlash")
      .GetVariable("$version")
      .replace(/\D+/g, ",")
      .match(/^,?(.+),?$/)[1];
    // other browsers
  } catch (e) {
    try {
      // @ts-ignore
      if (navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin) {
        // @ts-ignore
        return (
          // @ts-ignore
          navigator.plugins["Shockwave Flash"].description
            .replace(/\D+/g, ",")
            .match(/^,?(.+),?$/)[1]
        );
      }
    } catch (e) {}
  }
  return "0,0,0";
}

export function isFlashSupported(): "ok" | "old" | "ruffle" | "no" {
  const version = getFlashVersion().split(",").shift();
  if (version > 8) {
    // @ts-ignore
    if (navigator.plugins?.["Shockwave Flash"]?.filename.includes("ruffle"))
      return "ruffle";
    return "ok";
  }
  if (version === "0") return "no";
  return "old";
}
