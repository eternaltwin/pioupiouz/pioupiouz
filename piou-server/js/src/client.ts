import "./tooltip";

import { FlashObject } from "./flashobject";
import {
  createPlaySwf,
  createEditorSwf,
  createPreviewSwf,
  createReplaySwf,
} from "./utils";
import { isFlashSupported } from "./detectFlash";
(window as any).client = {
  createPlaySwf,
  createEditorSwf,
  createPreviewSwf,
  createReplaySwf,
  FlashObject,
  isFlashSupported,
};
