export function measureStart(func: Function) {
  performance.mark("start_" + func.name);
}

export function measureEnd(func: Function) {
  performance.mark("end_" + func.name);
  performance.measure(func.name, "start_" + func.name, "end_" + func.name);
  const measure = performance.getEntriesByName(func.name)[0];
  console.log(measure);
  performance.clearMeasures(func.name);
}
