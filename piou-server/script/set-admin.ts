import { NestFactory } from "@nestjs/core";
import { AppModule } from "../src/app.module";
import { Repository } from "typeorm";
import { User } from "../src/users/entities/user.entity";
import { getRepositoryToken } from "@nestjs/typeorm";

async function setAdmin(id: string) {
  // Create a new NestJS application context
  const appContext = await NestFactory.createApplicationContext(AppModule);

  // Get the user repository
  const userRepository = appContext.get(
    getRepositoryToken(User),
  ) as Repository<User>;

  try {
    // Update the user's admin status
    const user = await userRepository.findOneBy({ id: id });
    if (!user) {
      console.error(`User with id ${id} not found`);
      return;
    }
    console.log(user);
    if (user.isAdmin) {
      console.log(`User ${user.displayName} is already an admin`);
      return;
    }
    user.isAdmin = true;
    await userRepository.save(user);
    console.log(`User ${user.displayName} is now an admin`);
  } catch (error) {
    console.error("Error updating user admin status", error);
  } finally {
    // await appContext.close();
    process.exit(0);
  }
}

if (process.argv.length < 3) {
  console.error("Usage: node set-admin.js <id>");
  process.exit(1);
}

setAdmin(process.argv[2] as string);
