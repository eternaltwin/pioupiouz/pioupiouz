import { NestFactory } from "@nestjs/core";
import { AppModule } from "../src/app.module";
import { Repository } from "typeorm";
import { User } from "../src/users/entities/user.entity";
import { getRepositoryToken } from "@nestjs/typeorm";
import * as bcrypt from "bcrypt";

async function setUsernamePassword(id: string, username: string, password: string) {
  // Create a new NestJS application context
  const appContext = await NestFactory.createApplicationContext(AppModule);

  // Get the user repository
  const userRepository = appContext.get(
    getRepositoryToken(User),
  ) as Repository<User>;

  try {
    // Update the user's admin status
    const user = await userRepository.findOneBy({ id: id });
    if (!user) {
      console.error(`User with id ${id} not found`);
      return;
    }
    console.log(user);
    user.userName = username;
    user.password = await bcrypt.hash(password, 10);

    await userRepository.save(user);
    console.log(`User ${user.displayName} updated successfully`);
  } catch (error) {
    console.error("Error updating user", error);
  } finally {
    // await appContext.close();
    process.exit(0);
  }
}

if (process.argv.length < 5) {
  console.error("Usage: node set-admin.js <id> <username> <password>");
  process.exit(1);
}

setUsernamePassword(
  process.argv[2] as string,
  process.argv[3] as string,
  process.argv[4] as string,
);
