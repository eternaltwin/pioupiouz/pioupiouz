import {DataSource} from 'typeorm';
import dotenv from 'dotenv';
import configLoader from "./src/configuration"

dotenv.config({path: process.env.NODE_ENV !== "production" ? "../.env.development" : "../.env.production"});

const config = configLoader()

export default new DataSource({
    type: 'postgres',
    host: config.database.host,
    port: config.database.port,
    username: config.database.username,
    password: config.database.password,
    database: config.database.name,
    "entities": [
        "./src/**/entities/*.entity.ts"
    ],
    "synchronize": false,
    "migrations": [
        "src/migrations/*.ts"
    ],
    "migrationsTableName": "migrations_TypeORM"
});