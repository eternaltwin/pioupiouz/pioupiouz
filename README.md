# PiouPiouZ

```bash
# First, be sure to install deps with yarn
yarn
```

## Launch piou-server

This will launch the backend of PiouPiouZ

```bash
# Start a Postgres DB
docker compose up db -d
# Build everything
yarn build
# Setup the DB
yarn workspace server db:migrate

# Start server
yarn workspace server start:dev
```

```
Server: https://localhost:3000/
```

## Generate Flash SWF

- Install [Motion Types](https://github.com/motion-twin/mtypes/)

```bash
cd flash

make # Generate play.swf, editor.swf and loader.swf
```

Compiled SWF are included in the repository.

# Motion Twin

This repository use some source files provided by Motion Twin.
