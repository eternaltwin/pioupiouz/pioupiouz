# PiouPiouZ Flash

Compilation only works on Windows !

- Install Make for Windows
- Add `mtypes` folder to your PATH

```bash
make # Will compile all swfs and move them to piou-server
```

## PioupiouZ Loader

Since we doesn't have the PioupiouZ Loader source files, this is my MotionTypes implementation, based of deobfuscation of original `loader.swf`.

## PioupiouZ client

This is the MotionTwin PioupiouZ client, from their source code. (slightly modified)

### How to make a standalone build

- `cd game`
- Set your level in `src/TutoData.mt`
- Set `STAND_ALONE` to `true` in `src/Game.mt`
- Run `swfmake.exe -f standAloneMake.xml`
- Your standalone build is `swf/pioupiouz.swf`

## PioupiouZ Editor

This is the MotionTwin PioupiouZ editor, based on their source code. (slightly modified)
