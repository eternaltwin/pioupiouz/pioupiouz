class Loader
{
  static var WIDTH = 622;
  static var HEIGHT = 424;
  static var ___dev_pioupiouz_com = "$xNOpZUjm:AeNLIAOTqZhMxyRY4bCkE_MMUj";
  static var ___www_pioupiouz_com = "$xNOpZUjm:_wKLIAOTqZhMxyRY4bCkElD5hR";
  static var ___data_pioupiouz_com = "$xNOpZUjm:AaPpI3VZsWqWDvaYiv:bF95c3qB";
  static var ___www_pioupiouz_com2 = "$xNOpZUjm:_wKLIAOTqZhMxyRY4bCkElD5hR";
  static var URL_KEY = "$RJrjk05eeJrzp5Pazre7z9an788baz61kBKJ1EZ4";

  var mc:MovieClip;
  var loading: MovieClip;
  var game: MovieClip;

  var _mid: String;
  var _swf: String;
  var _key: String;
  var _playId: String;
  var lang: String;
  var gameInst: 'a;

  var counter:int;

  function new(initMc:MovieClip)
  {
    Stage.align = "$LT".substring(1);
    counter = 0;
    mc = initMc;

    // if (Std.getVar(Std.getRoot(), "$data".substring(1)) == "2") {
    // ___www_pioupiouz_com = ___data_pioupiouz_com;
    // }

    initMachineId();
    if (initBaseUrl() == false)
    {
      return null;
    }
    if (redirect(null) == true)
    {
      return null;
    }
    _swf = Std.getVar(Std.getRoot(), "$swf".substring(1));
    lang = Std.getVar(Std.getRoot(), "$lang");

    if (lang == null)
    {
      lang = "$en".substring(1);
    }

    if (Std.getVar(Std.getRoot(), "$mode") == null)
    {
      error("Game mode error");
      return null;
    }

    // if (Std.getVar(Std.getRoot(), "$mode") == 2)
    // {
		// 	Cs.mcw = 200
		// 	Cs.mch = 140
    // }

    initLoader();
    Log.setColor(16711680);
    return null;
  }

  function init(mc) {} 

  function redirect(___url)
  {
    var v3 = Std.getRoot();
    if (___url == null)
    {
      ___url = Std.getVar(v3, "$redirect".substring(1));
    }
    if (___url != null)
    {
      var v4 = new LoadVars();
			Std.cast(v4)[Std.cast("$mid".substring(1))] = _mid;
      v4.send(___url, "_self", "GET");
      return true;
    }
    return false;
  }

  function initLoader()
  {
    game = Std.createEmptyMC(mc, 1);
    attachLoading();
    // var v2 = loading._url;

    // if (v2.substr(0, ___www_pioupiouz_com.length) != ___www_pioupiouz_com) {
    // error("Le serveur de fichiers n\"as pas été trouvé.');
    // return null;
    // }

    var me = this;
    var v3 = new MovieClipLoader();
    v3.onLoadError = fun(_, msg)
    {
      me.error("Loading error : " + msg);
    };

    v3.onLoadInit = fun(_)
    {
      me.loadDone();
    };

    // var v4 = ___www_pioupiouz_com + _swf;
    var v4 = _swf;
    if (!v3.loadClip(v4, game))
    {
      error("Loading init error");
    }
  }

  function loadDone()
  {
    Log.trace("SWF loaded");
    Std.setGlobal("startGame", downcast(Std).callback(this, "startGame"));
    Std.setGlobal("exitGame", downcast(Std).callback(this, "exitGame"));
    Std.setGlobal("loadLevel", downcast(Std).callback(this, "loadLevel"));
    Std.setGlobal("saveLevel", downcast(Std).callback(this, "saveLevel"));
    gameInst = Std.getVar(game, "Manager");
    gameInst.init(game);
    if (___www_pioupiouz_com2 == "")
    {
      gameInst.main();
    }
    this.main = downcast(Std).callback(this, "mainOrGame");
  }

  function startGame()
  {
    Log.trace("Start game")
    loading.removeMovieClip();
    this.main = downcast(Std).callback(this, "mainGame");
  };

  function loadLevel(callback)
  {
    var self = this;
    var v2 = new LoadVars();
    v2.onData = fun(data)
    {
      var v3 = new Xml(data);
      v3.ignoreWhite = true;

      if (Std.getVar(Std.getRoot(), "$mode") != v3.firstChild.nextSibling.get("$mode".substring(1)))
      {
        self.error("Game mode error");
        return null;
      }
      self._playId = v3.firstChild.nextSibling.get ("$playId".substring(1));
      self._key = v3.firstChild.nextSibling.get ("$key".substring(1));
      if (self._key == null)
      {
        self._key = "";
      }
      callback(v3);
    };

    // v2.load(___www_pioupiouz_com2 + Std.cast(Std.getRoot()).$loadUrl);
    v2.load(Std.cast(Std.getRoot()).$loadUrl);
  };

  function exitGame(data)
  {
    var v2 = new Codec(_key);
    var v3 = new LoadVars();
    data.$pid = _playId;
    Std.cast(v3)[Std.cast("$pid")] = _playId;
    Std.cast(v3)[Std.cast("$data")] = v2.encode(data);
    v3.sendAndLoad(Std.cast(Std.getRoot()).$endUrl, v3, "_self");
    v3.onData = fun(data)
    {
      (new LoadVars()).send(data.substring(4), "_self", "GET");
    };

    return v3;
  };

  function saveLevel(str, callback)
  {
    var v3 = Std.cast(Std.getRoot()).$saveUrl;
    var v4 = new LoadVars();
    Std.cast(v4)[Std.cast("$act".substring(1))] = Std.cast(Std.getRoot()).$saveAct;
    Std.cast(v4)[Std.cast("$data")] = Std.cast(str);
    v4.onData = fun(data)
    {
      callback(data);
    };

    v4.sendAndLoad(v3, v4, "POST");
    return v4;
  };

  function checkBaseUrl()
  {
    return true;
  };

  function decodeUrl(url)
  {
    var v3 = new Codec(URL_KEY);
    url = v3.decode(Std.cast(url.substring(1)));
    url = Tools.replace(url, "YY", ".");
    url = Tools.replace(url, "XX", ":");
    url = Tools.replace(url, "_", "/");
    return url;
  };

  function initBaseUrl()
  {
    if (___www_pioupiouz_com2 == null)
    {
      ___www_pioupiouz_com2 = "";
      return checkBaseUrl();
    }
    if (Std.getVar(Std.getRoot(), "$vs".substring(1)) == "1")
    {
      ___www_pioupiouz_com2 = ___dev_pioupiouz_com;
      ___www_pioupiouz_com = ___dev_pioupiouz_com;
    }
    ___www_pioupiouz_com2 = decodeUrl(___www_pioupiouz_com2);
    ___www_pioupiouz_com = decodeUrl(___www_pioupiouz_com);
    return ___www_pioupiouz_com2 != null;
  };

  function error(msg)
  {
    Log.trace("error() : " + msg);
    downcast(loading).error.text = msg;
  };

  function initMachineId()
  {
    var v2 = SharedObject.getLocal("$mid");
    _mid = Std.cast(v2.data).$v;
    if (_mid.length != 8)
    {
      _mid = "";
      var v3;
      for (v3 = 0; v3 < 8; v3++)
      {
        _mid += Codec.BASE64.charAt(1 + Std.random(63));
      }
      Std.cast(v2.data).$v = _mid;
    }
  };

  function attachLoading()
  {
    loading = Std.attachMC(mc, "mcLoading", 2);
    loading._x = Stage.width / 2;
    loading._y = Stage.height / 2;
    loading.gotoAndStop(Std.toString(1));
    if (lang == "$fr".substring(1))
    {
      loading.gotoAndStop("1");
    }
    if (lang == "$en".substring(1))
    {
      loading.gotoAndStop("2");
    }
    if (lang == "$es".substring(1))
    {
      loading.gotoAndStop("3");
    }
  };

  function reboot()
  {
    return null;
    // Unrechable code
    // ++counter;
    // if (counter % 30 == 0) {
    // var v2 = loading._currentframe;
    // ++v2;
    // if (v2 > loading._totalframes) {
    // v2 = 1;
    // }
    // loading.gotoAndStop(Std.toString(v2));
    // }
  };

  function main()
  {
    if (loading != null)
    {
      reboot();
    }
  };

  function mainOrGame()
  {
    if (loading != null)
    {
      reboot();
    }
    gameInst.main();
  };

  function mainGame()
  {
    gameInst.main();
  };
} 
