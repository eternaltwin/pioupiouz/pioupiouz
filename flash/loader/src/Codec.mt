class Codec {
      static var IDCHARS = "$uasxIintfo";
      static var BASE64 = ":_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

      var key: String;
      var pkey: int;
      var mask: int;
      var crc: int;
      var str: String;

      function new(key) {
        key = key;
        mask = 0;
        var v3;
        for (v3 = 0; v3 < key.length; v3++) {
          mask *= 51;
          mask += key.charCodeAt(v3);
          mask &= -1;
        }
      };
      
      function makeCrcString() {
        return BASE64.charAt(crc & 63) + BASE64.charAt(crc >> 6 & 63) + BASE64.charAt(crc >> 12 & 63) + BASE64.charAt(crc >> 18 & 63) + BASE64.charAt(crc >> 24 & 63);
      };

      function encode(o: 'a) {
        pkey = 0;
        crc = 0;
        str = "";
        encodeAny(o);
        str += makeCrcString();
        return str;
      };

      function decode(s): 'a {
        pkey = 0;
        crc = 0;
        str = s;
        var v3 = decodeAny();
        if (str != makeCrcString()) {
          return null;
        }
        return v3;
      };

      function writeStr(s) {
        var v3;
        for (v3=0; v3 < s.length; v3++) {
          writeChar(s.charAt(v3));
        }
      };

      function writeChar(c) {
        var v3 = BASE64.indexOf(c, 0);
        if (v3 == -1) {
          str += c;
          return null;
        } else {
          var v4 = v3 ^ mask >> pkey & 63;
          crc *= 51;
          crc += v4;
          crc ^= -1;
          str += BASE64.charAt(v4);
        }
        pkey += 6;
        if (pkey >= 28) {
          pkey -= 28;
        }
        return null;
      };

      function readChar() {
        var v2 = str.charAt(0);
        var v3 = BASE64.indexOf(v2, 0);
        str = str.substring(1);
        if (v3 == -1) {
          return v2;
        }
        crc *= 51;
        crc += v3;
        crc ^= -1;
        v3 ^= mask >> pkey & 63;
        pkey += 6;
        if (pkey >= 28) {
          pkey -= 28;
        }
        return BASE64.charAt(v3);
      };

      function readStr() {
        var v2 = "";
        while (true) {
          var v3 = readChar();
          if (v3 == null) {
            return null;
          }
          if (v3 == ":") {
            break;
          }
          v2 += v3;
        }
        return v2;
      };

      function encodeArray(a) {
        writeStr(Std.toString(a.length));
        writeStr(":");
        var v3;
        for (v3 = 0; v3 < a.length; v3++) {
          encodeAny(a[v3]);
        }
      };

      function encodeObject(o) {
        var me = this;
        downcast(Std).forin(o, fun(k) {
          me.writeStr(k);
          me.writeStr(":");
          me.encodeAny(o[Std.cast(k)]);
        });
        writeStr(":");
      };

      function encodeAny(o: 'a) {
        if (o == null) {
          writeStr(IDCHARS.charAt(1));
        } else {
          if (downcast(Std)._instanceof(o, Array)) {
            writeStr(IDCHARS.charAt(2));
            encodeArray(Std.cast(o));
          } else {
            switch (downcast(Std)._typeof(o)) {
              case "string":
                writeStr(IDCHARS.charAt(3));
                writeStr(Std.cast(o));
                writeStr(":");
                break;
              case "number":
                var v3 = o;
                if (Std.isNaN(Std.cast(v3))) {
                  writeStr(IDCHARS.charAt(4));
                } else {
                  if (v3 == downcast(Std).infinity) {
                    writeStr(IDCHARS.charAt(5));
                  } else {
                    if (v3 == Std.cast(-downcast(Std).infinity)) {
                      writeStr(IDCHARS.charAt(6));
                    } else {
                      v3 = Std.cast(Std.parseInt(Std.cast(v3), 10));
                      writeStr(IDCHARS.charAt(7));
                      if (Std.cast(v3) < 0) {
                        writeStr(IDCHARS.charAt(1));
                      }
                      writeStr(Std.toString(Math.abs(Std.cast(v3))));
                      writeStr(":");
                    }
                  }
                }
                break;
              case "boolean":
                if (Std.cast(o) == true) {
                  writeStr(IDCHARS.charAt(Std.cast(o)));
                } else {
                  writeStr(IDCHARS.charAt(9));
                }
                break;
              default:
                writeStr(IDCHARS.charAt(10));
                encodeObject(Std.cast(o));
            }
          }
        }
      };

      function decodeArray() {
        var v3 = int(readStr());
        var v4 = new Array();
        var v2;
        for (v2 = 0; v2 < v3; v2++) {
          v4.push(decodeAny());
        }
        return v4;
      };

      function decodeObject() {
        var v2 = {};
        while (true) {
          var v3 = readStr();
          if (v3 == null) {
            return null;
          }
          if (v3 == "") {
            break;
          }
          var v4 = decodeAny();
          Std.cast(v2)[Std.cast(v3)] = v4;
        }
        return v2;
      };

      function decodeAny(): 'a {
        var v2 = readChar();
        switch (v2) {
          case IDCHARS.charAt(1):
            return Std.cast(null);
          case IDCHARS.charAt(2):
            return Std.cast(decodeArray());
          case IDCHARS.charAt(3):
            return Std.cast(readStr());
          case IDCHARS.charAt(4):
            return Std.cast(0 * null);
          case IDCHARS.charAt(5):
            return Std.cast(downcast(Std).infinity);
          case IDCHARS.charAt(6):
            return Std.cast(-downcast(Std).infinity);
          case IDCHARS.charAt(7):
            var v3 = readChar();
            var v4 = readStr();
            var v5;
            if (v3 == IDCHARS.charAt(1)) {
              v5 = Std.cast(-Std.parseInt(v4, 10));
            } else {
              v5 = Std.cast(Std.parseInt(v3 + v4, 10));
            }
            return v5;
          case IDCHARS.charAt(8):
            return Std.cast(true);
          case IDCHARS.charAt(9):
            return Std.cast(false);
          case IDCHARS.charAt(10):
            return Std.cast(decodeObject());
        }
        return null;
      };
}