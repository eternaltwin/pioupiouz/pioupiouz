//---------------------------------------------------------
// Code generated from Random.mt

Random = function(n) {
    if( (n == null) ) n = 1;
    this.seedLow = n;
    this.seedHigh = 0;

}
;
Random.prototype.setSeed = function(n) {
    this.seedLow = n;
    this.seedHigh = 0;

}
;
Random.prototype.random = function(max) {
    var low = 0xDB6DB6DB;
    var high = 0xA65AEC2F;
    var slow = int(this.seedLow * low);
    var shigh = int(this.seedHigh * low + this.seedLow * high);
    this.seedLow = slow + 1;
    this.seedHigh = shigh;
    return (this.seedLow >>> 3) % max;
}
;
Random.prototype.rand = function() {
    return this.random(1000) / 1000;
}
;


