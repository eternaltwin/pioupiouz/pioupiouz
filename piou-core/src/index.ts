export function helloPiou(piou: string) {
  console.log("Hello, " + piou);
}

import {readXMLLevel, decodeLevelData, decodePiouPiouzLevel} from "./encoding"
import fs from "fs"

let test  = fs.readFileSync("response_1688217299834.xml").toString();
let xml = readXMLLevel(test);
fs.writeFileSync("level.json", JSON.stringify(decodePiouPiouzLevel(xml!)));
