import {
  DeobfuPiouPiouzLevel,
  ExitGameData,
  LevelData,
  PiouPiouzLevel,
  SoluceData,
  XMLHeader,
  XMLOptions,
} from "./types";
import { parse as mtBonParse, emit as mtBonEmit } from "@eternalfest/mtbon";
import { CodecV1_5 } from "mt-codec";
import { XMLBuilder, XMLParser } from "fast-xml-parser";

import { apply_mapping, unapply_mapping } from "./obfu_mappings";

export const decodeLevelData = (
  data: string,
  applyMappings = false
): LevelData => {
  const obj = mtBonParse(data);
  if (applyMappings) apply_mapping(obj);
  return obj;
};

export const encodeLevelData = (
  data: LevelData,
  applyMappings = false
): string => {
  if (applyMappings) unapply_mapping(data);
  return mtBonEmit(data);
};

export const decodeSoluceData = (
  soluce: string,
  applyMappings = false
): SoluceData => {
  const obj = mtBonParse(soluce);
  if (applyMappings) apply_mapping(obj);
  return obj;
};

export const encodeSoluceData = (
  soluce: SoluceData,
  applyMappings = false
): string => {
  if (applyMappings) unapply_mapping(soluce);
  return mtBonEmit(soluce);
};

export const decodeExitGame = (
  data: string,
  key: string = ""
): ExitGameData => {
  const codec = new CodecV1_5(key);
  const obj: ExitGameData = codec.deserialize(data);
  return obj;
};

export const decodePiouPiouzLevel = (
  obfuLevel: PiouPiouzLevel
): DeobfuPiouPiouzLevel => {
  return {
    ...obfuLevel,
    data: decodeLevelData(obfuLevel.data),
    soluce: obfuLevel.soluce ? decodeSoluceData(obfuLevel.soluce) : undefined,
  };
};

export const encodePiouPiouzLevel = (
  deobfuLevel: DeobfuPiouPiouzLevel
): PiouPiouzLevel => {
  return {
    ...deobfuLevel,
    data: encodeLevelData(deobfuLevel.data),
    soluce: deobfuLevel.soluce
      ? encodeSoluceData(deobfuLevel.soluce)
      : undefined,
  };
};

const safeParseInt = (str: string): number | null =>
  isNaN(parseInt(str)) || isNaN(Number(str)) ? null : Number(str);

export const readXMLLevel = (xml: string): PiouPiouzLevel | null => {
  const xmlData = new XMLParser(XMLOptions).parse(xml);

  for (const key in xmlData) {
    if (key === "?xml") continue;
    const level = xmlData[key];
    for (const lkey in level) {
      let res: number | null = safeParseInt(level[lkey]);
      if (res !== null) level[lkey] = res;
    }
    return level;
  }
  return null;
};

export const readJSONLevel = (json: string): DeobfuPiouPiouzLevel | null => {
  return JSON.parse(json);
};

export const buildJSONLevel = (level: DeobfuPiouPiouzLevel): string => {
  return JSON.stringify(level, null, 2);
};

export const buildXMLLevel = (level: PiouPiouzLevel): string => {
  const xmlData = new XMLBuilder(XMLOptions).build({
    level: level,
  });
  return XMLHeader + xmlData.toString();
};
