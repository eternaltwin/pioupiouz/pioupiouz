const process = (
  obj: any,
  mappings: typeof ObfuMappings | typeof ClearMappings
): any => {
  if (obj && typeof obj === "object") {
    if (Array.isArray(obj)) {
      for (let item of obj) process(item, mappings);
      return;
    }

    for (let key of Object.keys(obj)) {
      process(obj[key], mappings);
      if (mappings[key]) {
        obj[mappings[key]] = obj[key];
        delete obj[key];
      }
    }
  }
};

const objectFlip = (obj: any): any => {
  const ret: any = {};
  Object.keys(obj).forEach((key) => {
    ret[obj[key]] = key;
  });
  return ret;
};

const ClearMappings = {
  tiles: "1*gU6",
  id: "*B",
  platforms: "7=joK",
  w: " (",
  rid: "9*I",
  rot: "9;I",
  piou: "{)59",
  out: ";xD",
  action: ";8,PR",
  caisse: "9cgEI(",
  did: "49-",
  history: "3X]M5",
};

const ObfuMappings = objectFlip(ClearMappings);

export const apply_mapping = (obj: any): any => {
  process(obj, ObfuMappings);
};

export const unapply_mapping = (obj: any): any => {
  process(obj, ClearMappings);
};
