import { X2jOptions, XmlBuilderOptions } from "fast-xml-parser";

// fast-xml-parser doesn't add the line feed, and it crash pioupiouz swf.
export const XMLHeader = "<?xml version='1.0' encoding='UTF-8'?>\n";

export enum PlayMode {
  Replay = 0,
  Normal = 1,
  Preview = 2,
  Edit = 3,
}

export enum Medal {
  None = 0,
  Dead = 1,
  Silver = 2,
  Gold = 3,
  Star = 4,
}

export interface PiouPiouZFlashVars {
  swf?: string; // link to play.swf
  $playId?: number; // unused
  $try?: 0 | 1; // unused
  $lang?: "en" | "fr" | "es";
  $loadUrl?: string; // link to xml
  $endUrl?: string; // endpoint to send the exit request
  $help?: 0 | 1; // display some infos in the pause menu, else hide them
  $mode?: PlayMode; // game mode (must be the same as inside XML)
  $saveUrl?: string; // Used for the level editor
  $saveAct?: string; // Used for the level editor
  $exitUrl?: string; // Used for the level editor
  $selfUrl?: string; // Used for the level editor
  $testUrl?: string; // Used for the level editor
}

export type PiouPiouzLevel<DataFormat = string, SoluceFormat = string> = {
  _piouz: number;
  _mode: PlayMode;
  _time: number;
  _title: string;
  _id: number;
  _playId: number;
  _status?: Medal;
  data: DataFormat;
  soluce?: SoluceFormat;
  tuto?: string;
  cards?: string;
};

export type DeobfuPiouPiouzLevel = PiouPiouzLevel<LevelData, SoluceData>;

export interface SaveGameData<LevelData = string> {
  $data: LevelData;
  act: string;
}

export interface ExitGameData<SoluceData = string> {
  $time: number;
  $piouz: number;
  $pid: number;
  $theme: number;
  $soluce: SoluceData | null;
}

export type DeobfuExitGameData = ExitGameData<SoluceData>;

export interface LevelData {
  tiles: Array<{ x: number; y: number; id: number }>;
  platforms: Array<{
    x: number;
    y: number;
    w: number;
    rid?: number;
    rot?: number;
    list?: Array<[number, number]>;
  }>;
  piou: Array<[number, number, number]>; // [x, y, direction]
  action: Array<[number, number]>; // [id, number]
  out: Array<[number, number]>; // [x, y]
  caisse: Array<[number, number, number, number]>; // [x, y, id, number]
  size: [number, number];
  did: number;
}

export interface SoluceData {
  // Click: [btime, x, y] | Select action: [btime, -1, id]
  history: Array<[number, -1 | number, number]>;
}

export const XMLOptions: Partial<X2jOptions> & Partial<XmlBuilderOptions> = {
  ignoreAttributes: false,
  attributeNamePrefix: "_",
  format: true,
};
