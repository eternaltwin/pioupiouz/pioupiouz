FROM node:18

WORKDIR /app

ENV NODE_ENV production

COPY .yarnrc.yml ./
COPY .yarn/releases/* ./.yarn/releases/

COPY yarn.lock package.json ./
COPY piou-core/package*.json ./piou-core/
COPY piou-server/package*.json ./piou-server/
COPY piou-server/js/package*.json ./piou-server/js/

RUN yarn install --immutable

COPY . .
RUN yarn build
RUN chmod +x /app/entrypoint.sh

EXPOSE 3000

ENTRYPOINT [ "/app/entrypoint.sh" ]
CMD [ "node", "build/main.js" ]
